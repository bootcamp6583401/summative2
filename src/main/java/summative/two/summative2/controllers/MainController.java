package summative.two.summative2.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import summative.two.summative2.models.User;
import summative.two.summative2.services.impl.UserServiceImpl;

@Controller
public class MainController {

    @Autowired
    UserServiceImpl userService;

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult userRes, String retypePassword, Model model) {
        String errorMessage = "";
        System.out.println("PASSWORD LENGTH " + user.getPassword().length());

        Optional<User> userByEmail = userService.findByEmail(user.getEmail());
        Optional<User> userByPhone = userService.findByPhone(user.getPhone());

        if (userByEmail.isPresent() || userByPhone.isPresent()) {
            errorMessage += "Phone number/email has already been taken. Choose another.";
        }
        if (userRes.hasErrors()) {

            for (ObjectError error : userRes.getAllErrors()) {
                errorMessage += error.getDefaultMessage();
                errorMessage += "\n";
            }
        }
        if (!user.getPassword().equals(retypePassword)) {
            System.out.println("PASSWORD DO NOT MATCH");
            errorMessage += "Passwords do not match";
        }

        if (user.getPassword().length() > 12 || user.getPassword().length() < 8) {
            errorMessage += "Password must be between 8 and 12";
        }

        if (errorMessage.length() > 0) {
            model.addAttribute("registerError", errorMessage);
            return "register";

        }
        userService.save(user);
        System.out.println("SAVING USER");
        return "login";
    }

    @GetMapping("/register")
    public String registerForm(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @GetMapping("/forgot-password")
    public String forgotPasswordForm(Model model) {
        return "forgot-password";
    }

    @PostMapping("/forgot-password")
    public String handleForgotPassword(@RequestParam String email, @RequestParam String newPassword,
            @RequestParam String retypedPassword,
            RedirectAttributes ra, Model model) {

        Optional<User> userRes = userService.findByEmail(email);

        if (!newPassword.equals(retypedPassword)) {
            model.addAttribute("error", "Password mismatch");
            return "forgot-password";
        }

        if (!userRes.isPresent()) {
            model.addAttribute("error", "Email not found");
            return "forgot-password";
        } else {
            User user = userRes.get();
            user.setPassword(newPassword);
            userService.save(user);
        }

        return "login";
    }
}
