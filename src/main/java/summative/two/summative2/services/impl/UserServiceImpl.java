package summative.two.summative2.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import summative.two.summative2.models.User;
import summative.two.summative2.repositories.UserRepository;
import summative.two.summative2.services.UserService;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user) {
        user.setPhone(user.getPhone().replaceAll("-", ""));
        if (user.getPhone().charAt(0) == '0') {
            user.setPhone("+62" + user.getPhone().substring(1));
        }
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

}
