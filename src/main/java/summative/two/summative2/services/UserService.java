package summative.two.summative2.services;

import java.util.Optional;

import summative.two.summative2.models.User;

public interface UserService {
    void save(User user);

    Optional<User> findByEmail(String email);

    Optional<User> findByPhone(String phone);
}
