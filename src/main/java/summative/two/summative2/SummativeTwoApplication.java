
package summative.two.summative2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SummativeTwoApplication {

	public static void main(String[] args) throws Throwable {
		SpringApplication.run(SummativeTwoApplication.class, args);
	}

}
