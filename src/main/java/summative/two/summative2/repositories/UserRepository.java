package summative.two.summative2.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import summative.two.summative2.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByPhone(String phone);

}
